﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleSMS.ModelWork
{
    public class ModelSettings
    {
        public ModelSettings()
        {
            UniformSettings = new UniformDistributionSettings();
            NormalSettings = new NormalDistributionSettings();
        }
        public UniformDistributionSettings UniformSettings { get; set; }
        public NormalDistributionSettings NormalSettings { get; set; }
        public double ProbabilityOfReturn { get; set; }
        public double DeltaT { get; set; }
        public double SystemWorkingTimeLimit { get; set; }
    }


    public class UniformDistributionSettings
    {
        public double Left { get; set; }
        public double Right { get; set; }

    }

    public class NormalDistributionSettings
    {
        public double Mean { get; set; }
        public double Deviation { get; set; }
    }

}
