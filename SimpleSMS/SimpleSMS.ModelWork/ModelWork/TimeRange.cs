﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleSMS.ModelWork
{
    class TimeRange
    {
        public TimeRange() { }

        public TimeRange(double fromTime, double toTime)
        {
            FromTime = fromTime;
            ToTime = toTime;
        }

        public double FromTime { get; set; }
        public double ToTime { get; set; }
    }
}
