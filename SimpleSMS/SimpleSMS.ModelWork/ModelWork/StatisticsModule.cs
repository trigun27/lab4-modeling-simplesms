﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleSMS.ModelWork
{
    class StatisticsModule
    {
        private double systemWorkingTimeLimit;

        public StatisticsModule(double workingTimeLimit)
        {
            systemWorkingTimeLimit = workingTimeLimit;
        }

        public bool DoContinueModeling(double currentTime)
        {
            return currentTime < systemWorkingTimeLimit;
        }
    }
}
