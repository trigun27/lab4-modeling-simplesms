﻿using System;

namespace SimpleSMS.ModelWork
{
    public class ModelingEvent
    {
        public ModelingEvent(Action<Request> callFunc, double nextCallTime, Request request)
        {
            CallFunc = callFunc;
            NextCallTime = nextCallTime;
            Request = request;
        }

        public double NextCallTime { get; set; }
        public Action<Request> CallFunc { get; set; }
        public Request Request { get; set; }


    }

    public class ModelingEventArgs : EventArgs
    {
        public ModelingEventArgs(Action<Request> callFunc, double nextCallTime, Request request)
        {
            ModelEvent = new ModelingEvent(callFunc, nextCallTime,request);
        }
        public ModelingEvent ModelEvent { get; set; }
    }


}
