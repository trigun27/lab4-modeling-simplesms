﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SimpleSMS.ModelWork;
using SimpleSMS.ModelWork.Entities;

namespace SimpleSMS.ModelWork
{
    public class ModelManager
    {

        #region Members
        private static volatile ModelManager _instanceManager;
        private static object   _syncRoot = new object();

        //Schema
        private InfoSource infoSource;
        private Storage storage;
        private ServingMachine servingMachine;

        //Model Patterns
        private List<ModelingEvent> modelingEvents;
        private StatisticsModule statisticsModule;
        private double currentTime;
        private double dt;

        #endregion

        #region Constructors
        private ModelManager() { }

        public static ModelManager InstanceManager
        {
            get
            {
                if (_instanceManager == null)
                {
                    lock (_syncRoot)
                    {
                        if (_instanceManager == null)
                            _instanceManager = new ModelManager();
                    }
                }
                return _instanceManager;
            }
        }


        public void Initialize(ModelSettings settings)
        {
            CreateScheme(settings);
            CreateStatistics(settings);
            ConnectEventsInSheme();
            InitializeParameters(settings);
        }

        private void CreateScheme(ModelSettings settings)
        {
            servingMachine = new ServingMachine(settings.NormalSettings);
            storage = new Storage(servingMachine);
            infoSource = new InfoSource(storage,settings.ProbabilityOfReturn,settings.UniformSettings);
        }

        private void CreateStatistics(ModelSettings settings)
        {
            statisticsModule = new StatisticsModule(settings.SystemWorkingTimeLimit);
        }

        private void ConnectEventsInSheme()
        {
            infoSource.InfoSourceEvent += AddModelingEvent;
            servingMachine.EndOfServingRequest += AddModelingEvent;
            servingMachine.RequestSeved += infoSource.OnRequestProcessed;
        }

        private void InitializeParameters(ModelSettings settings)
        {
            modelingEvents = new List<ModelingEvent>();
            currentTime = 0;
            dt = settings.DeltaT;
        }

        #endregion

        #region Methods

        public void ModelAllSystem()
        {
            infoSource.CreateRequest();
            while (statisticsModule.DoContinueModeling(currentTime))
            {
                ExecuteCurrentEvents();
                currentTime += dt;
            }
      
        }

        private void ExecuteCurrentEvents()
        {
            List<ModelingEvent> currentEvents = GetCurrentEvents();
            foreach (var modelingEvent in currentEvents)
            {
                modelingEvent.CallFunc(modelingEvent.Request);
                modelingEvents.Remove(modelingEvent);
            }
        }

        private List<ModelingEvent> GetCurrentEvents()
        {
            List<ModelingEvent> currentEvents =
                modelingEvents.Where(ev => ev.NextCallTime >= currentTime && ev.NextCallTime <= currentTime+dt).ToList();
            return currentEvents;
        }

        public void AddModelingEvent(object sender, ModelingEventArgs modelingEventArgs)
        {
            var modelEvent = modelingEventArgs.ModelEvent;
            modelEvent.NextCallTime += currentTime;

            var nearestEventIndex = modelingEvents.FindIndex(ev => modelEvent.NextCallTime < ev.NextCallTime);
            if (nearestEventIndex != -1)
            {
                modelingEvents.Insert(nearestEventIndex, modelEvent);
            }
            else
            {
                modelingEvents.Add(modelEvent);
            }
        }

        public int GetStorageQueueLength()
        {
            return storage.GetQueueLength();
        }

        #endregion





    }
}
