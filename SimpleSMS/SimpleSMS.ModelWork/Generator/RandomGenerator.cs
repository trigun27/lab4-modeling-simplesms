﻿using System;

namespace SimpleSMS.ModelWork
{
    class RandomGenerator
    {
        private static MT19937 RandomMT = new MT19937();
        /// <summary>
        /// Genaration PRN, which would be represent time of return
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static double GetUniform(double a, double b)
        {
            return a + RandomMT.genrand_real1() * (b - a);
        }

        /// <summary>
        /// Generation PRN, which would be represent time
        /// </summary>
        /// <param name="mean"></param>
        /// <param name="standardDeviation"></param>
        /// <returns></returns>
        public static double GetNormal(double mean, double standardDeviation)
        {
            return mean + standardDeviation * GetStandardNormal();
        }

        
        /// <summary>
        /// Get normal (Gaussian) random sample with mean 0 and standard deviation 1
        /// Use Box-Muller algorithm 
        /// http://qps.ru/yTzcV
        /// </summary>
        /// <returns></returns>
        private static double GetStandardNormal()
        {
            double u1 = RandomMT.genrand_real1();
            double u2 = RandomMT.genrand_real1();
            double r = Math.Sqrt(-2.0 * Math.Log(u1));
            double theta = 2.0 * Math.PI * u2;
            return r * Math.Sin(theta);
        }
    }
}
