﻿using System.Collections.Generic;

namespace SimpleSMS.ModelWork.Entities
{


    class Storage
    {
        private Queue<Request> requests;
        private int queLength;
        private ServingMachine machine;

        /// <summary>
        /// Create Storage, which contains queue of requests
        /// </summary>
        /// <param name="servingMachine"></param>
        public Storage(ServingMachine servingMachine)
        {
            machine = servingMachine;
            machine.RequestSeved += MachineOnRequestServed;
            requests = new Queue<Request>();
            queLength = 0;
        }

        public void MachineOnRequestServed(object sender, Request request)
        {
            SendNextRequestToservingMachine();
        }

        public void AddRequest(Request request)
        {
            if (IsMemoryEmpty() && machine.IsFree())
            {
                machine.BeginServeRequest(request);
            }
            else
            {
                Enqueue(request);
            }
        }

        public int GetQueueLength()
        {
            return queLength;
        }

        private void Enqueue(Request request)
        {
            requests.Enqueue(request);
            queLength++;
        }

        private Request Dequeue()
        {
            return requests.Dequeue();
        }

        private bool IsMemoryEmpty()
        {
            return requests.Count == 0;
        }

        private void SendNextRequestToservingMachine()
        {
            if (!IsMemoryEmpty() && machine.IsFree())
            {
                var request = Dequeue();
                machine.BeginServeRequest(request);
            }
        }

    }
}
