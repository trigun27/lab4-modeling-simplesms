﻿namespace SimpleSMS
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtbFrom = new System.Windows.Forms.TextBox();
            this.txtbTo = new System.Windows.Forms.TextBox();
            this.txtbVariance = new System.Windows.Forms.TextBox();
            this.txtbMean = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtbTimeUnit = new System.Windows.Forms.TextBox();
            this.txtbProbReturn = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtbTimeWork = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtbResult = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.btnStart = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtbTo);
            this.groupBox1.Controls.Add(this.txtbFrom);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(288, 100);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Равномерное распределение";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txtbVariance);
            this.groupBox2.Controls.Add(this.txtbMean);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Location = new System.Drawing.Point(12, 118);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(288, 100);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Нормальное распределение (ОА)";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.txtbTimeWork);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.txtbTimeUnit);
            this.groupBox3.Controls.Add(this.txtbProbReturn);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Location = new System.Drawing.Point(12, 224);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(288, 125);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Доп. параметры";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.btnStart);
            this.groupBox4.Controls.Add(this.txtbResult);
            this.groupBox4.Controls.Add(this.label8);
            this.groupBox4.Location = new System.Drawing.Point(306, 12);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(174, 337);
            this.groupBox4.TabIndex = 3;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Результаты";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(20, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "От";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 67);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(22, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "До";
            // 
            // txtbFrom
            // 
            this.txtbFrom.Location = new System.Drawing.Point(33, 26);
            this.txtbFrom.Name = "txtbFrom";
            this.txtbFrom.Size = new System.Drawing.Size(126, 20);
            this.txtbFrom.TabIndex = 1;
            this.txtbFrom.Text = "1";
            // 
            // txtbTo
            // 
            this.txtbTo.Location = new System.Drawing.Point(33, 64);
            this.txtbTo.Name = "txtbTo";
            this.txtbTo.Size = new System.Drawing.Size(126, 20);
            this.txtbTo.TabIndex = 1;
            this.txtbTo.Text = "2";
            // 
            // txtbVariance
            // 
            this.txtbVariance.Location = new System.Drawing.Point(33, 62);
            this.txtbVariance.Name = "txtbVariance";
            this.txtbVariance.Size = new System.Drawing.Size(126, 20);
            this.txtbVariance.TabIndex = 4;
            this.txtbVariance.Text = "0,5";
            // 
            // txtbMean
            // 
            this.txtbMean.Location = new System.Drawing.Point(33, 24);
            this.txtbMean.Name = "txtbMean";
            this.txtbMean.Size = new System.Drawing.Size(126, 20);
            this.txtbMean.TabIndex = 5;
            this.txtbMean.Text = "4";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 65);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(22, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "DX";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(7, 27);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(23, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "MX";
            // 
            // txtbTimeUnit
            // 
            this.txtbTimeUnit.Location = new System.Drawing.Point(135, 62);
            this.txtbTimeUnit.Name = "txtbTimeUnit";
            this.txtbTimeUnit.Size = new System.Drawing.Size(126, 20);
            this.txtbTimeUnit.TabIndex = 8;
            this.txtbTimeUnit.Text = "0,01";
            // 
            // txtbProbReturn
            // 
            this.txtbProbReturn.Location = new System.Drawing.Point(135, 24);
            this.txtbProbReturn.Name = "txtbProbReturn";
            this.txtbProbReturn.Size = new System.Drawing.Size(126, 20);
            this.txtbProbReturn.TabIndex = 9;
            this.txtbProbReturn.Text = "0";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(7, 65);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(89, 13);
            this.label5.TabIndex = 6;
            this.label5.Text = "Единица верени";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(7, 27);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(122, 13);
            this.label6.TabIndex = 7;
            this.label6.Text = "Вероятность возврата";
            // 
            // txtbTimeWork
            // 
            this.txtbTimeWork.Location = new System.Drawing.Point(135, 99);
            this.txtbTimeWork.Name = "txtbTimeWork";
            this.txtbTimeWork.Size = new System.Drawing.Size(126, 20);
            this.txtbTimeWork.TabIndex = 11;
            this.txtbTimeWork.Text = "40";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(7, 102);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(80, 13);
            this.label7.TabIndex = 10;
            this.label7.Text = "Время работы";
            // 
            // txtbResult
            // 
            this.txtbResult.Location = new System.Drawing.Point(11, 60);
            this.txtbResult.Name = "txtbResult";
            this.txtbResult.Size = new System.Drawing.Size(150, 20);
            this.txtbResult.TabIndex = 11;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(8, 29);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(153, 13);
            this.label8.TabIndex = 10;
            this.label8.Text = "Оптимальная длина очереди";
            // 
            // btnStart
            // 
            this.btnStart.Location = new System.Drawing.Point(11, 86);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(150, 77);
            this.btnStart.TabIndex = 12;
            this.btnStart.Text = "Моделеровать";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(487, 357);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtbTo;
        private System.Windows.Forms.TextBox txtbFrom;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox txtbVariance;
        private System.Windows.Forms.TextBox txtbMean;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox txtbTimeWork;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtbTimeUnit;
        private System.Windows.Forms.TextBox txtbProbReturn;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.TextBox txtbResult;
        private System.Windows.Forms.Label label8;
    }
}

