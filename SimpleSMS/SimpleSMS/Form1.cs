﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using SimpleSMS.ModelWork;


namespace SimpleSMS
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }


        private ModelSettings GetSettings()
        {
            try
            {
                var settings = new ModelSettings();
                settings.UniformSettings.Left = Convert.ToDouble(txtbFrom.Text);
                settings.UniformSettings.Right = Convert.ToDouble(txtbTo.Text);
                settings.NormalSettings.Mean = Convert.ToDouble(txtbMean.Text);
                settings.NormalSettings.Deviation = Convert.ToDouble(txtbVariance.Text);
                settings.ProbabilityOfReturn = Convert.ToDouble(txtbProbReturn.Text);
                settings.DeltaT = Convert.ToDouble(txtbTimeUnit.Text);
                settings.SystemWorkingTimeLimit = Convert.ToDouble(txtbTimeWork.Text);

                return settings;
            }
            catch (Exception)
            {
                MessageBox.Show("Ошибка считывания данных");
            }
            return null;

        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            var settings = GetSettings();
            if (settings != null)
            {
                ModelManager.InstanceManager.Initialize(settings);
                ModelManager.InstanceManager.ModelAllSystem();

                var queueLength = ModelManager.InstanceManager.GetStorageQueueLength();
                txtbResult.Text = queueLength.ToString();
            }

        }





    }
}
